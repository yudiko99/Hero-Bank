package com.hero.atm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hero.atm.feign.BankInterface;
import com.hero.atm.pojo.request.LoginRequest;

@Service
public class LoginService {
	@Autowired
	private BankInterface bankInterface;
	
	public String login(String nama, Integer pin) {
		LoginRequest logReq = new LoginRequest();
		logReq.setNama(nama);
		logReq.setPin(pin);
		return bankInterface.login(logReq);
	}
}

	
