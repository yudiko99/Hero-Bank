package com.hero.atm.pojo.request;

public class LoginRequest {

	private String nama;
	private int pin;

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public int getPin() {
		return pin;
	}

	public void setPin(int pin) {
		this.pin = pin;
	}

	@Override
	public String toString() {
		return "LoginRequest [nama=" + nama + ", pin=" + pin + "]";
	}

	public LoginRequest(String nama, int pin) {
		super();
		this.nama = nama;
		this.pin = pin;
	}

	public LoginRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

}
