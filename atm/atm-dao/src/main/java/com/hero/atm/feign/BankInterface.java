package com.hero.atm.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hero.atm.pojo.request.LoginRequest;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

@FeignClient("bank")
public interface BankInterface {
	@HystrixCommand
	@HystrixProperty(name="hystrix.command.default.execution.isolation.thread.timeoutInMilisecond", value="3000000")
	@RequestMapping(value="/bank/user/login", method=RequestMethod.POST)
	public String login(@RequestBody LoginRequest req);
	
}