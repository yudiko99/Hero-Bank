package com.hero.atm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hero.atm.pojo.request.LoginRequest;
import com.hero.atm.pojo.response.BaseResponse;
import com.hero.atm.service.LoginService;

@RestController
@RequestMapping("/user")
public class AtmController {
	@Autowired
	private LoginService loginService;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public BaseResponse Login(@RequestBody LoginRequest logReq) {
		String result = loginService.login(logReq.getNama(), logReq.getPin());
		BaseResponse response = new BaseResponse();
		if (result != null) {
			response.setResponseCode("00");
			response.setResponseDesc("Success");
		} else {
			response.setResponseCode("01");
			response.setResponseDesc("Failed");
		}
		return response;
	}

}
