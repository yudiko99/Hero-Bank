package com.hero.bank.controller;

import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.cache.CacheException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hero.bank.constant.RC;
import com.hero.bank.model.Transaksi;
import com.hero.bank.model.User;
import com.hero.bank.pojo.request.TransaksiRequest;
import com.hero.bank.pojo.request.TransferRequest;
import com.hero.bank.pojo.request.UserRequest;
import com.hero.bank.pojo.response.BaseResponse;
import com.hero.bank.pojo.response.Rekening;
import com.hero.bank.service.TransaksiService;
import com.hero.bank.service.UserService;

@RestController
@RefreshScope
@RequestMapping("/user")
@Produces(MediaType.APPLICATION_JSON)
public class BankController {

	@Autowired
	private UserService uService;
	@Autowired
	private TransaksiService tService;

	@RequestMapping("/print/{noRek}")
	public List<Transaksi> findByNoRek(@PathVariable Integer noRek) {
		// List<TransaksiResponse> list = new ArrayList<TransaksiResponse>();
		List<Transaksi> transactions = tService.findByNoRek(noRek);
		return transactions;
		/*
		 * TransaksiResponse response = new TransaksiResponse();
		 * 
		 * for (Transaksi trx : transactions) { response.setId(trx.getId());
		 * response.setNoRek(trx.getNoRek()); response.setJumlah(trx.getJumlah());
		 * response.setKet(trx.getKet()); response.setSaldo(trx.getSaldo());
		 * list.add(response); response = new TransaksiResponse(); } return list;
		 */
	}

	@POST
	@RequestMapping("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public BaseResponse addUser(@RequestBody UserRequest req) {
		BaseResponse response = new BaseResponse();
		try {
			User usr = new User();
			usr.setNama(req.getNama());
			usr.setNoRek(req.getNoRek());
			usr.setAlamat(req.getAlamat());
			usr.setTelp(req.getTelp());
			usr.setPin(req.getPin());
			uService.save(usr);
		} catch (Exception e) {
			e.printStackTrace();
			response.setResponseCode(RC.ERROR);
			response.setResponseDesc(e.getMessage());
			return response;
		}
		response.setResponseCode(RC.SUCCESS);
		response.setResponseDesc("SUCCESS");
		return response;
	}

	/*
	 * @POST
	 * 
	 * @RequestMapping("/saldo")
	 * 
	 * @Consumes(MediaType.APPLICATION_JSON) public String cekSaldo(@RequestBody
	 * TransaksiRequest req) { Transaksi saldo =
	 * tService.findFirstByNoRekOrderByIdDesc(req.getNoRek()); return
	 * "{Saldo Anda : " + String.valueOf(saldo.getSaldo()) + "}"; }
	 */
	@POST
	@RequestMapping("/saldo")
	@Consumes(MediaType.APPLICATION_JSON)
	public Rekening cekSaldo(@RequestBody TransaksiRequest req) {
		Transaksi trx = tService.findFirstByNoRekOrderByIdDesc(req.getNoRek());
		Rekening rekening = new Rekening();
		rekening.setNoRek(trx.getNoRek());
		rekening.setSaldo(trx.getSaldo());
		return rekening;
	}

	@POST
	@RequestMapping("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	public User findUser(@RequestBody UserRequest request) {
		return uService.login(request.getNama(), request.getPin());
	}

	@POST
	@RequestMapping("/deposit")
	@Consumes(MediaType.APPLICATION_JSON)
	public BaseResponse deposit(@RequestBody TransaksiRequest req) {
		BaseResponse response = new BaseResponse();
		try {
			Transaksi transaksiLast = tService.findFirstByNoRekOrderByIdDesc(req.getNoRek());
			Transaksi newTrx = new Transaksi();
			Long saldoTotal = transaksiLast.getSaldo() + req.getJumlah();
			newTrx.setNoRek(req.getNoRek());
			newTrx.setJumlah(req.getJumlah());
			newTrx.setKet("deposit");
			newTrx.setSaldo(saldoTotal);
			tService.save(newTrx);
		} catch (Exception e) {
			e.printStackTrace();
			response.setResponseCode(RC.ERROR);
			response.setResponseDesc(e.getMessage());
			return response;
		}
		response.setResponseCode(RC.SUCCESS);
		response.setResponseDesc("SUCCESS");
		return response;
	}

	@POST
	@RequestMapping("/withdraw")
	@Consumes(MediaType.APPLICATION_JSON)
	public BaseResponse withdraw(@RequestBody TransaksiRequest req) {
		BaseResponse response = new BaseResponse();
		try {
			Transaksi withdraw = tService.findFirstByNoRekOrderByIdDesc(req.getNoRek());
			Transaksi trx = new Transaksi();
			Long total = withdraw.getSaldo() - req.getJumlah();
			trx.setNoRek(req.getNoRek());
			trx.setJumlah(req.getJumlah());
			trx.setKet("withdraw");
			trx.setSaldo(total);
			tService.save(trx);
		} catch (Exception e) {
			e.printStackTrace();
			response.setResponseCode(RC.ERROR);
			response.setResponseDesc(e.getMessage());
			return response;
		}
		response.setResponseCode(RC.SUCCESS);
		response.setResponseDesc("SUCCESS");
		return response;
	}

	@POST
	@RequestMapping("/transfer")
	@Consumes(MediaType.APPLICATION_JSON)
	public BaseResponse transfer(@RequestBody TransferRequest tfReq) {
		BaseResponse response = new BaseResponse();
		try {
			Transaksi trxRekA = tService.findFirstByNoRekOrderByIdDesc(tfReq.getRekA());
			Transaksi trxRekB = tService.findFirstByNoRekOrderByIdDesc(tfReq.getRekB());
			Long saldoA = trxRekA.getSaldo();
			Transaksi newTrxA = new Transaksi();
			Transaksi newTrxB = new Transaksi();

			if (saldoA < tfReq.getJumlah()) {
				throw new Exception();
			}

			Long totalA = saldoA - tfReq.getJumlah();
			newTrxA.setNoRek(tfReq.getRekA());
			newTrxA.setJumlah(tfReq.getJumlah());
			newTrxA.setKet("Transfer ke : " + trxRekB.getNoRek());
			newTrxA.setSaldo(totalA);
			tService.save(newTrxA);

			Long totalB = trxRekB.getSaldo() + tfReq.getJumlah();
			newTrxB.setNoRek(tfReq.getRekB());
			newTrxB.setJumlah(tfReq.getJumlah());
			newTrxB.setKet("Transfer dari : " + newTrxA.getNoRek());
			newTrxB.setSaldo(totalB);
			tService.save(newTrxB);
		} catch (HibernateException e) {
			e.printStackTrace();
			response.setResponseCode(RC.ERROR);
			response.setResponseDesc("Query error");
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			response.setResponseCode(RC.ERROR);
			response.setResponseDesc(e.getMessage());
			return response;
		}
		response.setResponseCode(RC.SUCCESS);
		response.setResponseDesc("SUCCESS");
		return response;
	}

}
