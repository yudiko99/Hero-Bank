package com.hero.bank.pojo.request;

public class TransferRequest {

	private Integer rekA;
	private Integer rekB;
	private Long jumlah;

	public Integer getRekA() {
		return rekA;
	}

	public void setRekA(Integer rekA) {
		this.rekA = rekA;
	}

	public Integer getRekB() {
		return rekB;
	}

	public void setRekB(Integer rekB) {
		this.rekB = rekB;
	}

	public Long getJumlah() {
		return jumlah;
	}

	public void setJumlah(Long jumlah) {
		this.jumlah = jumlah;
	}

}
