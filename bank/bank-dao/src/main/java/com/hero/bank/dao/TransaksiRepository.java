package com.hero.bank.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.hero.bank.model.Transaksi;

public interface TransaksiRepository extends CrudRepository<Transaksi, Integer>{

	public abstract Transaksi findFirstByNoRekOrderByIdDesc(Integer noRek);
	
	public abstract List<Transaksi> findByNoRek(Integer noRek);
}
