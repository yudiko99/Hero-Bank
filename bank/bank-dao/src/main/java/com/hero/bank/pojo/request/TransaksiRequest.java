package com.hero.bank.pojo.request;

public class TransaksiRequest {

	
	private Integer noRek;
	private String ket;
	private Long jumlah;
	private Long saldo;
	public Integer getNoRek() {
		return noRek;
	}
	public void setNoRek(Integer noRek) {
		this.noRek = noRek;
	}
	public String getKet() {
		return ket;
	}
	public void setKet(String ket) {
		this.ket = ket;
	}
	public Long getJumlah() {
		return jumlah;
	}
	public void setJumlah(Long jumlah) {
		this.jumlah = jumlah;
	}
	public Long getSaldo() {
		return saldo;
	}
	public void setSaldo(Long saldo) {
		this.saldo = saldo;
	}
	
	
}
