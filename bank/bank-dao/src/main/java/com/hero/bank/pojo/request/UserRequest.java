package com.hero.bank.pojo.request;

public class UserRequest {

	private Integer noRek;
	private String nama;
	private String alamat;
	private Integer telp;
	private Integer pin;

	public Integer getNoRek() {
		return noRek;
	}

	public void setNoRek(Integer noRek) {
		this.noRek = noRek;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public Integer getTelp() {
		return telp;
	}

	public void setTelp(Integer telp) {
		this.telp = telp;
	}

	public Integer getPin() {
		return pin;
	}

	public void setPin(Integer pin) {
		this.pin = pin;
	}

	@Override
	public String toString() {
		return "UserRequest [noRek=" + noRek + ", nama=" + nama + ", alamat=" + alamat + ", telp=" + telp + ", pin="
				+ pin + "]";
	}
}
