package com.hero.bank.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_user")
public class User {
	@Id
	@Column(name = "no_rek", nullable = false, unique = true)
	private Integer noRek;

	@Column(name = "nama", nullable = false, length = 64)
	private String nama;

	@Column(name = "alamat", nullable = false, length = 64)
	private String alamat;

	@Column(name = "telp", nullable = false)
	private Integer telp;

	@Column(name = "pin", nullable = false)
	private Integer pin;

	public Integer getNoRek() {
		return noRek;
	}

	public void setNoRek(Integer noRek) {
		this.noRek = noRek;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public Integer getTelp() {
		return telp;
	}

	public void setTelp(Integer telp) {
		this.telp = telp;
	}

	public Integer getPin() {
		return pin;
	}

	public void setPin(Integer pin) {
		this.pin = pin;
	}

	@Override
	public String toString() {
		return "TbUser [noRek=" + noRek + ", nama=" + nama + ", alamat=" + alamat + ", telp=" + telp + ", pin=" + pin
				+ "]";
	}

}
