package com.hero.bank.pojo.response;

public class Rekening {

	private Integer noRek;
	private Long saldo;

	public Integer getNoRek() {
		return noRek;
	}

	public void setNoRek(Integer noRek) {
		this.noRek = noRek;
	}

	public Long getSaldo() {
		return saldo;
	}

	public void setSaldo(Long saldo) {
		this.saldo = saldo;
	}

	@Override
	public String toString() {
		return "Rekening [noRek=" + noRek + ", saldo=" + saldo + "]";
	}

}
