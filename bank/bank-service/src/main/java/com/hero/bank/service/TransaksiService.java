package com.hero.bank.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Service;

import com.hero.bank.dao.TransaksiRepository;
import com.hero.bank.model.Transaksi;
import com.hero.bank.util.CollectionHelper;

@EnableCaching
@Service
public class TransaksiService {

	@Autowired
	private TransaksiRepository tRepository;

	@Cacheable(value="transaksi.findAll", unless= "#result==null")
	@SuppressWarnings("unchecked")
	public List<Transaksi> findAll() {
		return CollectionHelper.iterToList(tRepository.findAll());
	}

	@CacheEvict(value="transaksi.save", allEntries=true, beforeInvocation=true)
	public Transaksi save(Transaksi trx) {
		return tRepository.save(trx);
	}

	@Cacheable(value="transaksi.findById", unless= "#result==null")
	public Transaksi findById(Integer id) {
		return tRepository.findById(id).get();
	}

//	public TbTransaksi findByNoRek(Integer noRek) {
//		return tRepository.findByNoRek(noRek);
//	}
	
	@Cacheable(value="transaksi.findByNoRek", unless= "#result==null")
	public List<Transaksi> findByNoRek(Integer noRek) {
		return tRepository.findByNoRek(noRek);
	}

	@Cacheable(value="transaksi.findByNoRekTop", unless= "#result==null")
	public Transaksi findFirstByNoRekOrderByIdDesc(Integer noRek) {
		return tRepository.findFirstByNoRekOrderByIdDesc(noRek);
	}

}
