package com.hero.heroeureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class HeroEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(HeroEurekaApplication.class, args);
	}
}
