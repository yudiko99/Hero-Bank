package com.hero.hazelcasthero;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HazelcastHeroApplication {

	public static void main(String[] args) {
		SpringApplication.run(HazelcastHeroApplication.class, args);
	}
}
